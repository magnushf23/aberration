use crate::{
    gpu_handler::GpuHandler,
    imagetexture::ImageTexture,
    renderer::{RenderParameters, Renderer},
};
use time::Instant;
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

pub struct Program {
    pub gpu: GpuHandler,
    pub renderer: Renderer,
    pub texture: ImageTexture,
    pub dimension: [u32; 2],
}
impl Program {
    pub async fn new(window: &Window) -> Self {
        let init_time = Instant::now();

        // Create simulation problem
        let width = 1200;
        let height = 600;
        let dimension = [width, height];

        // Initialize the gpu controller
        let gpu = GpuHandler::new(window).await;

        let render_params = RenderParameters { size: dimension };

        let texture = ImageTexture::new(&gpu, dimension, wgpu::TextureFormat::Rgba8Unorm);

        let renderer = Renderer::new(&gpu, &render_params, &texture);
        //let solver = Solver::new(&gpu);
        println!(
            "\nInitialisation done in {} seconds.\n",
            init_time.elapsed()
        );

        //gpu.surface.configure(&gpu.device, &gpu.config);

        Program {
            gpu,
            //solver,
            renderer,
            texture,
            dimension,
        }
    }

    pub fn input(&mut self, _event: &WindowEvent) -> bool {
        false
    }

    pub fn update(&mut self) {
        // Nothing
    }

    pub fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        // Create render output surface for sim visualization

        let output = self.gpu.surface.get_current_texture()?;
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self
            .gpu
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Compute & Render Encoder"),
            });

        // Update simulation
        for _iteration in 0..1 {
            //self.solver
            //    .next_step(&mut encoder, &self.texture.compute_bind_group);
        }

        // Render
        self.renderer
            .render(&mut encoder, &view, &self.texture.render_bind_group);

        self.gpu.queue.submit(std::iter::once(encoder.finish()));
        //self.gpu.queue.submit(Some(encoder.finish()));
        output.present();
        //println!("Renders...");
        Ok(())
    }
}

// Main run loop
pub async fn run() {
    {
        env_logger::init();
    };
    // Initialize event loop and window
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("Lattice Boltzmann Simulation")
        .build(&event_loop)
        .unwrap();

    // Build program
    let mut program = Program::new(&window).await;
    let mut stopwatch = Instant::now();
    let mut render_counter = 0;

    // Run
    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent {
            ref event,
            window_id,
        } if window_id == window.id() => {
            if !program.input(event) {
                match event {
                    WindowEvent::CloseRequested
                    | WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: ElementState::Pressed,
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    } => *control_flow = ControlFlow::Exit,
                    WindowEvent::Resized(physical_size) => {
                        program.gpu.resize(*physical_size);
                    }
                    WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                        // new_inner_size is &&mut so we have to dereference it twice
                        program.gpu.resize(**new_inner_size);
                    }
                    _ => {}
                }
            }
        }
        Event::RedrawRequested(window_id) if window_id == window.id() => {
            match program.render() {
                Ok(_) => {
                    render_counter += 1;
                }
                // Reconfigure the surface if lost
                Err(wgpu::SurfaceError::Lost | wgpu::SurfaceError::Outdated) => {
                    program.gpu.resize(program.gpu.size)
                }
                // The system is out of memory, we should probably quit
                Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                // All other errors (Outdated, Timeout) should be resolved by the next frame
                Err(e) => eprintln!("{:?}", e),
            }
        }
        Event::MainEventsCleared => {
            // RedrawRequested will only trigger once, unless we manually
            // request it.
            let elapsed_time = stopwatch.elapsed().as_seconds_f64();
            if elapsed_time >= 1. {
                println!(
                    "FPS: {:.2e} | Mean invocation runtime: {:.2e} s",
                    (render_counter as f64 / elapsed_time),
                    elapsed_time
                        / (program.dimension[0] * program.dimension[1]) as f64
                        / render_counter as f64,
                );
                render_counter = 0;
                stopwatch = Instant::now();
            }
            window.request_redraw();
        }
        _ => {}
    });
}
